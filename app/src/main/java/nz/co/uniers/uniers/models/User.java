package nz.co.uniers.uniers.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import nz.co.uniers.uniers.R;

/**
 * Created by yangyu on 13/08/15.
 * <p/>
 * This class models a user object which has sid, name and courses attributes.
 */
public class User {
    @SerializedName("sid")
    private final String sid;
    @SerializedName("name")
    private final String name;
    @SerializedName("courses")
    private final List<Course> courses;

    public User(String sid, String name, List<Course> courses) {
        this.sid = sid;
        this.name = name;
        this.courses = new ArrayList<>(courses);
    }

    public String getName() {
        return name;
    }

    public String getSid() {
        return sid;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void save(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.user_preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        Gson gson = new Gson();
        editor.putString(context.getString(R.string.user_key), gson.toJson(this));
        editor.apply();
    }

}
