package nz.co.uniers.uniers.utils;

/**
 * Created by yangyu on 5/08/15.
 * <p/>
 * This class contains a collection of urls which will be used to obtain lecture timetable.
 */
public final class UrlBuilder {
    private static final String HTTP_HOST_NAME = "http://my.vuw.ac.nz/";
    private static final String HTTPS_HOST_NAME = "https://my.vuw.ac.nz/";

    private static final String SIGN_IN_FRAGMENT = "cp/home/displaylogin";
    private static final String SIGN_IN_FAILED_FRAGMENT = "cp/home/login";
    private static final String HOME_FRAGMENT = "render.userLayoutRootNode.uP?uP_root=root";
    private static final String MY_STUDY_FRAGMENT = "tag.77e9e505bdb4ad72.render.userLayoutRootNode.uP?uP_root=root&uP_sparam=activeTab&activeTab=u12l1s8&uP_tparam=frm&frm=";
    private static final String TIMETABLE_FRAGMENT = "https://student-records.vuw.ac.nz/pls/webprod/bwskfshd.P_CrseSchd";

    public static String getSignInUrl() {
        return HTTPS_HOST_NAME + SIGN_IN_FRAGMENT;
    }

    public static String getSignInFailedUrl() {
        return HTTPS_HOST_NAME + SIGN_IN_FAILED_FRAGMENT;
    }

    public static String getHomeUrl() {
        return HTTP_HOST_NAME + HOME_FRAGMENT;
    }

    public static String getMyStudyUrl() {
        return HTTP_HOST_NAME + MY_STUDY_FRAGMENT;
    }

    public static String getTimetableUrl() {
        return TIMETABLE_FRAGMENT;
    }

}
