package nz.co.uniers.uniers.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nz.co.uniers.uniers.R;
import nz.co.uniers.uniers.models.Course;
import nz.co.uniers.uniers.models.ErrorType;
import nz.co.uniers.uniers.models.User;
import nz.co.uniers.uniers.utils.JSBuilder;
import nz.co.uniers.uniers.utils.UrlBuilder;
import nz.co.uniers.uniers.utils.Utils;

/**
 * Created by yangyu on 5/08/15.
 * <p/>
 * Landing page that enables sign in to my victoria.
 */
public class LandingActivity extends AppCompatActivity {
    @Bind(R.id.web_view)
    WebView webView;
    @Bind(R.id.username_et)
    EditText username;
    @Bind(R.id.username_error_tv)
    TextView usernameError;
    @Bind(R.id.password_et)
    EditText password;
    @Bind(R.id.password_error_tv)
    TextView passwordError;
    @Bind(R.id.sign_in_btn)
    ActionProcessButton signIn;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Bind(R.id.connect_to_internet_btn)
    Button connectToInternet;

    private Handler handler;
    private Runnable runnable;

    public interface AuthenticationListener {
        void onSuccess(User user);

        void onError(ErrorType errorType);
    }

    private final AuthenticationListener listener = new AuthenticationListener() {
        @Override
        public void onSuccess(User user) {
            user.save(LandingActivity.this);
        }

        @Override
        public void onError(ErrorType errorType) {
            Snackbar.make(webView, errorType.getErrorMsg(), Snackbar.LENGTH_LONG).show();
            if (errorType != ErrorType.INTERNET_DISCONNECTED) {
                webView.loadUrl(UrlBuilder.getSignInUrl());
                username.setText("");
                password.setText("");
            } else {
                if (progressBar.getVisibility() == View.GONE) {
                    username.setVisibility(View.GONE);
                    password.setVisibility(View.GONE);
                    signIn.setVisibility(View.GONE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
                connectToInternet.setVisibility(View.VISIBLE);
            }
            signIn.setText(R.string.sign_in_text);
            signIn.setProgress(0);
        }
    };

    private final BroadcastReceiver connectivityChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (connectToInternet.getVisibility() == View.VISIBLE && Utils.isInternetAvailable(context)) {
                connectToInternet.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                webView.loadUrl(UrlBuilder.getSignInUrl());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (Utils.compareUrl(webView.getUrl(), UrlBuilder.getSignInFailedUrl())) {
                    listener.onError(ErrorType.INCORRECT_CREDENTIALS);
                }
            }
        };

        setFont("AvenirNextLTPro-Demi.otf");

        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            webView.getSettings().setSavePassword(false);
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.addJavascriptInterface(new MyJSInterface(), "android");
        webView.loadUrl(UrlBuilder.getSignInUrl());
    }

    private void setFont(String font) {
        final Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/" + font);

        username.setTypeface(tf);
        usernameError.setTypeface(tf);
        password.setTypeface(tf);
        passwordError.setTypeface(tf);
        signIn.setTypeface(tf);
        connectToInternet.setTypeface(tf);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(connectivityChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(connectivityChangeReceiver);
    }

    @OnClick(R.id.sign_in_btn)
    public void signIn() {
        Utils.hideSoftKeyboard(this);

        usernameError.setVisibility(View.GONE);
        passwordError.setVisibility(View.GONE);

        if (!Utils.validateEditText(username)) {
            usernameError.setVisibility(View.VISIBLE);
        } else if (!Utils.validateEditText(password)) {
            passwordError.setVisibility(View.VISIBLE);
        } else if (!Utils.isInternetAvailable(this)) {
            listener.onError(ErrorType.INTERNET_DISCONNECTED);
        } else {
            signIn.setText(R.string.authenticating_text);
            signIn.setProgress(50);
            webView.loadUrl(JSBuilder.getSignInJS(Utils.getEditText(username), Utils.getEditText(password)));
        }
    }

    @OnClick(R.id.connect_to_internet_btn)
    public void connectToInternet() {
        startActivity(new Intent(Settings.ACTION_SETTINGS));
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            // explicitly check if internet is available
            if (!Utils.isInternetAvailable(LandingActivity.this)) {
                listener.onError(ErrorType.INTERNET_DISCONNECTED);
            } else {
                listener.onError(ErrorType.UNKNOWN_SERVER_ERROR);
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (Utils.compareUrl(url, UrlBuilder.getSignInUrl())) {
                if (connectToInternet.getVisibility() == View.GONE) {
                    progressBar.setVisibility(View.GONE);
                    username.setVisibility(View.VISIBLE);
                    password.setVisibility(View.VISIBLE);
                    signIn.setVisibility(View.VISIBLE);
                }
            } else if (Utils.compareUrl(url, UrlBuilder.getSignInFailedUrl())) {
                handler.postDelayed(runnable, 1000);
            } else if (Utils.compareUrl(url, UrlBuilder.getMyStudyUrl())) {
                signIn.setText(R.string.getting_timetable_text);
                webView.loadUrl(JSBuilder.getLectureTimetableJS());
            } else if (Utils.compareUrl(url, UrlBuilder.getTimetableUrl())) {
                webView.loadUrl(JSBuilder.getProcessTimetableJS());
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Utils.compareUrl(url, UrlBuilder.getHomeUrl())) {
                webView.loadUrl(UrlBuilder.getMyStudyUrl());
            }
            return false;
        }

    }

    private class MyJSInterface {

        @JavascriptInterface
        @SuppressWarnings("unused")
        public void extractTimetable(String html) {
            // 1. parse HTML into a Document
            final Document document = Jsoup.parse(html);

            // 2. get student id and full name
            final Element header = document.select("div.staticheaders").first();
            final String idAndName = header.textNodes().get(0).text().trim();
            final String sid = idAndName.split("\\s+")[0];
            final String name = idAndName.replace(sid, "");

            // 3. get lecture timetable
            final ArrayList<Course> courses = new ArrayList<>();
            final Elements elements = document.select("td.ddlabel");
            if (!elements.isEmpty()) {
                int id = 0;
                for (Element element : elements) {
                    element.attr("id", String.valueOf(id++)); // add a unique id to each record
                }
                for (Element element : elements) {
                    int day = element.elementSiblingIndex();
                    if (element.text().contains("pm")) {
                        day++;
                    }

                    List<TextNode> textNodes = element.child(0).textNodes();
                    courses.add(new Course(textNodes.get(0).text().trim(), day, textNodes.get(2).text().trim(), textNodes.get(3).text().trim()));
                }
            }
            listener.onSuccess(new User(sid, name, courses));
        }

    }

}
