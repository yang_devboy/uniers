package nz.co.uniers.uniers.models;

import nz.co.uniers.uniers.R;

/**
 * Created by yangyu on 11/08/15.
 * <p/>
 * This class represents a set of error types when signing in and retrieving timetable.
 */
public enum ErrorType {
    NO_ERROR(-1, -1),
    INTERNET_DISCONNECTED(1, R.string.no_internet_error),
    INCORRECT_CREDENTIALS(2, R.string.incorrect_credentials),
    UNKNOWN_SERVER_ERROR(3, R.string.unknown_server_error);

    private final int errorCode;
    private final int errorMsg;

    ErrorType(int errorCode, int errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public int getErrorMsg() {
        return errorMsg;
    }

}
