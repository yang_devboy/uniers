package nz.co.uniers.uniers.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yangyu on 13/08/15.
 * <p/>
 * This class models a course object which has name, day, time and room attributes.
 */
public class Course {
    @SerializedName("name")
    private final String name;
    @SerializedName("day")
    private final int day;
    @SerializedName("time")
    private final String time;
    @SerializedName("room")
    private final String room;

    public Course(String name, int day, String time, String room) {
        this.name = name;
        this.day = day;
        this.time = time;
        this.room = room;
    }

    public String getName() {
        return name;
    }

    public int getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }

    public String getRoom() {
        return room;
    }

}
