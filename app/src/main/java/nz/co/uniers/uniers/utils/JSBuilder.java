package nz.co.uniers.uniers.utils;

/**
 * Created by yangyu on 10/08/15.
 * <p/>
 * This class builds essential java scripts so as to obtain lecture timetable.
 */
public final class JSBuilder {
    private static final String OUTER_JS = "javascript: %s";

    public static String getSignInJS(String username, String password) {
        return String.format(OUTER_JS, "document.getElementById('user').value = '" + username + "';" +
                "document.getElementById('pass').value = '" + password + "';" +
                "var event = document.createEvent('MouseEvents');" +
                "event.initEvent('click', true, false);" +
                "document.getElementsByClassName('newHPjun12-subbutton')[0].dispatchEvent(event);");
    }

    public static String getLectureTimetableJS() {
        return String.format(OUTER_JS, "var elements = document.getElementsByTagName('a');" +
                "var element = null;" +
                "for (var i = 0, l = elements.length; i < l; i++) {" +
                "if (elements[i].innerHTML === 'My Lecture Timetable') {" +
                "element = elements[i];" +
                "break;" +
                "}}" +
                "var event = document.createEvent('MouseEvents');" +
                "event.initEvent('click', true, false);" +
                "element.dispatchEvent(event);");
    }

    public static String getProcessTimetableJS() {
        return String.format(OUTER_JS, "window.android.extractTimetable('<html>' + document.getElementsByTagName('html')[0].innerHTML + '</html>')");
    }

}
