package nz.co.uniers.uniers.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by yangyu on 10/08/15.
 * <p/>
 * This class provides a set of general purpose utility methods.
 */
public final class Utils {

    public static boolean isInternetAvailable(Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean validateEditText(EditText editText) {
        return !TextUtils.isEmpty(getEditText(editText));
    }

    public static String getEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static boolean compareUrl(String url1, String url2) {
        return url1.equalsIgnoreCase(url2);
    }

    public static void hideSoftKeyboard(Activity context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
    }

}
